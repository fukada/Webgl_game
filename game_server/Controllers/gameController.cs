﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.IO;
using System.Text;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;


namespace game_server.Controllers
{
    public class gameController : ApiController
    {
        private static Player[] player = new Player[4];
        private static bool setup = false;
        public static int num = 0;
         /*
        public IEnumerable<int> Get()
        {
            num++;
            return new int;
        }
        */
        public object Get()
        {
            if (!setup)
            {
                player[0] = new Player();
                player[1] = new Player();
                player[2] = new Player();
                player[3] = new Player();
                setup = true;
            }
            if (num == 4) return -1;
            return num++;
        }
        public object Get(int id)
        {
            return id;
        }

        public object Post([FromBody]string value)
        {
            return "";
        }

        public string Post(int id, [FromBody]string value)
        {
            if (id == -1) return "0";
            var pos = value.Split(',');
            player[id].x = double.Parse(pos[0]);
            player[id].y = double.Parse(pos[1]);
            player[id].z = double.Parse(pos[2]);
            var eneid = (id + 1) % 2;
            var res = (num - 1).ToString();
            for (int i = 0; i < num; i++)
                if (id != i)
                    res += "," + string.Join<double>(",", player[i].getpos());
            return res;
        }

        //ファイルの保存
        private string open_file(string url)
        {
            using (StreamReader sr = new StreamReader(url, Encoding.GetEncoding("utf-8")))
            {
                return sr.ReadToEnd();
            }
        }

        //ファイルの書き込み
        private void save_file(string url, string text)
        {
            using (StreamWriter writer = new StreamWriter(url, true, Encoding.GetEncoding("utf-8")))
            {
                writer.Write(text);
            }
        }

        public static object getObjectFromJson(string jsonString, Type t)
        {
            var serializer = new DataContractJsonSerializer(t);
            var jsonBytes = Encoding.Unicode.GetBytes(jsonString);
            var sr = new MemoryStream(jsonBytes);
            return serializer.ReadObject(sr);
        }
    }

    class Player
    {
        public double x;
        public double y;
        public double z;
        public double[] getpos()
        {
            return new double[] { x, y, z };
        }
    }

}