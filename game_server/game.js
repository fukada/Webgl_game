﻿/// <reference path="index.html"/>
var a,myid;
window.onload = function () {
    myid = location.hash = read_file("api/game/" + location.hash.replace("#", ""), false);

    a = new Three(window.innerWidth, window.innerHeight);
    a.canvas.style.position = "absolute";
    a.canvas.style.top = "0px";
    a.canvas.style.left = "0px";
    a.disable_culling();
    a.enable_depth_test();
    var wall = a.create_quadrangle(10, 100, 0, 1, 0);
    var floor = a.create_quadrangle(100, 100, 0.6, 0.6, 0.6);

    var kabe = a.create_quadrangle(10, 10, 1, 1, 1);
    var p = player();
    var obj1 = a.create_sphere();
    var obj2 = a.create_rectangular(3, 3, 3, 0.5, 1, 1);
    var mypos, enepos=[0];
    a.canvas.addEventListener("click", function () { a.lock_pointer(); }, false);
    (function () {
        mypos = a.chmviewbymouse();
        a.clear_canvas(0, 0, 0);

        post_request("api/game/" + myid, mypos.join(","), true, function (data) {
            enepos = data.slice(1,-1).split(",");
        });
        for (var i = 0; i < enepos[0]; i++) {
            p.init();
            p.translate(enepos[i * 3 + 1], enepos[i * 3 + 2], enepos[i * 3 + 3]);
            p.draw();
        }

        floor.draw();
        wall.init();
        wall.translate(100, 0, 0);
        wall.rotate(90, [0, 0, 1]);
        wall.draw();
        wall.init();
        wall.translate(0, 0, 0);
        wall.rotate(90, [0, 0, 1]);
        wall.draw();
        wall.init();
        wall.translate(0, 0, 100);
        wall.rotate(90, [1, 0, 0]);
        wall.rotate(90, [0, 1, 0]);
        wall.draw();
        wall.init();
        wall.translate(0, 0, 0);
        wall.rotate(90, [1, 0, 0]);
        wall.rotate(90, [0, 1, 0]);
        wall.draw();

        kabe.init();
        kabe.translate(10, 0, 10);
        kabe.rotate(90, [1, 0, 0]);
        kabe.rotate(90, [0, 1, 0]);

        kabe.draw();

        obj1.translate(10, 2, 0);
        obj1.draw();

        obj2.init();
        obj2.draw();

        a.flush();

        setTimeout(arguments.callee, 1000 / 60);
    })();
}
var player = function () {

    // 頂点属性を格納する配列
    var position = [
         0.0, 0.0, 0.0,
         1.0, 1.0, 0.0,
         0.0, 1.0, 1.0,
         -1.0, 1.0, 0.0,
         0.0, 1.0, -1.0,
         0.0, 2.0, 0.0
    ];
    var normal = [
         0.0, -1.0, 0.0,
         1.0, 0.0, 0.0,
         0.0, 0.0, 1.0,
         -1.0, 0.0, 0.0,
         0.0, 0.0, -1.0,
         0.0, 1.0, 0.0,
    ];
    var color = [
        1.0, 0.0, 0.0, 1.0,
        1.0, 1.0, 0.0, 1.0,
        0.0, 1.0, 1.0, 1.0,
        1.0, 1.0, 0.0, 1.0,
        0.0, 1.0, 1.0, 1.0,
        0.0, 0.0, 1.0, 1.0
    ];

    // 頂点のインデックスを格納する配列
    var index = [
        0, 1, 2,
        0, 2, 3,
        0, 3, 4,
        0, 4, 1,
        2, 1, 5,
        3, 2, 5,
        4, 3, 5,
        1, 4, 5
    ];

    return a.create_sonota(position, color, normal, index);
}

var pillar = function () {

}
/************************************************************/
//ファイルの読み込み
function read_file(url,/*非同期処理するかどうか*/fasyn, callback) {
    var httpObj = new XMLHttpRequest();
    httpObj.open("GET", url,fasyn);
    httpObj.send(null);
    callback = callback || function (data) { return data; };
    if (fasyn) {
        httpObj.onreadystatechange = function () {
            if (httpObj.readyState == 4) {
                callback(httpObj.responseText);
            }
        }
        return undefined;
    } else {
        if (httpObj.readyState == 4) {
            return httpObj.responseText;
        }
    }
}
function post_request(url, str,/*非同期処理するかどうか*/fasyn, callback) {
    var httpObj = new XMLHttpRequest();
    httpObj.open("POST", url, fasyn);
    httpObj.setRequestHeader("Content-Type", "application/json");
    httpObj.send(JSON.stringify(str));
    callback = callback || function (data) { return data; };
    if (fasyn) {
        httpObj.onreadystatechange = function () {
            if (httpObj.readyState == 4) {
                callback(httpObj.responseText);
            }
        }
        return undefined;
    } else {
        if (httpObj.readyState == 4) {
            return httpObj.responseText;
        }
    }
}
/************************************************************/
