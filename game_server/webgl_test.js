﻿/// <reference path="index.html"/>

var Three = function (/*number*/width,/*number*/height, parentElement) {

    this.canvas = document.createElement("canvas");
    var c = this.canvas;
    c.width = width;
    c.height = height;
    (parentElement || document.getElementsByTagName("body").item(0)).appendChild(c);

    // webglコンテキストを取得
    var gl = c.getContext('webgl') || c.getContext('experimental-webgl');

    // 頂点シェーダとフラグメントシェーダの生成
    var v_shader = create_shader('vs');
    var f_shader = create_shader('fs');

    // プログラムオブジェクトの生成とリンク
    var prg = create_program(v_shader, f_shader);

    // attributeLocationを配列に取得
    var attLocation = new Array();
    attLocation[0] = gl.getAttribLocation(prg, 'position');
    attLocation[1] = gl.getAttribLocation(prg, 'normal');
    attLocation[2] = gl.getAttribLocation(prg, 'color');
    attLocation[3] = gl.getAttribLocation(prg, 'textureCoord');

    // attributeの要素数を配列に格納
    var attStride = new Array();
    attStride[0] = 3;
    attStride[1] = 3;
    attStride[2] = 4;
    attStride[3] = 2;

    // uniformLocationを配列に取得
    var uniLocation = new Array();
    uniLocation[0] = gl.getUniformLocation(prg, 'mvpMatrix');
    uniLocation[1] = gl.getUniformLocation(prg, 'mMatrix');
    uniLocation[2] = gl.getUniformLocation(prg, 'invMatrix');
    uniLocation[3] = gl.getUniformLocation(prg, 'lightPosition');
    uniLocation[4] = gl.getUniformLocation(prg, 'eyeDirection');
    uniLocation[5] = gl.getUniformLocation(prg, 'ambientColor');
    uniLocation[6] = gl.getUniformLocation(prg, 'texture');
    uniLocation[8] = gl.getUniformLocation(prg, 'ftexture');
    uniLocation[9] = gl.getUniformLocation(prg, 'vertexAlpha');

    /****************************************************/

    // minMatrix.js を用いた行列関連処理
    // matIVオブジェクトを生成
    var m = new matIV();

    // 各種行列の生成と初期化
    var vMatrix = m.identity(m.create());
    var pMatrix = m.identity(m.create());
    var tmpMatrix = m.identity(m.create());
    var mvpMatrix = m.identity(m.create());

    this.change_view = function (f) {
        f(m);
        // ビュー×プロジェクション座標変換行列
        m.lookAt([5.0, 5.0, 5.0], [0, 0, 0], [0, 1, 0], vMatrix);
        m.perspective(45, c.width / c.height, 0.1, 100, pMatrix);
        m.multiply(pMatrix, vMatrix, tmpMatrix);
    };

    // 視点ベクトル
    var eyeDirection = [-20, -20, -20];
    //環境光の色
    var ambientColor = [0.2, 0.2, 0.2, 1.0];
    // 点光源の位置
    var lightPosition = [100.0, 100.0, 100.0];

    gl.uniform3fv(uniLocation[3], lightPosition);
    gl.uniform3fv(uniLocation[4], eyeDirection);
    gl.uniform4fv(uniLocation[5], ambientColor);

    var keystate = new Array(10), scrollTop;

    window.addEventListener("keydown", function (e) {
        switch (e.keyCode) {
            case 65://A
                keystate[0] = true;
                break;
            case 87://W
                keystate[1] = true;
                break;
            case 68://D
                keystate[2] = true;
                break;
            case 83://S
                keystate[3] = true;
                break;
            case 81://Q
                keystate[4] = true;
                break;
            case 90://Z
                keystate[5] = true;
                break;
            case 37://←
                keystate[6] = true;
                break;
            case 38://↑
                keystate[7] = true;
                break;
            case 39://→
                keystate[8] = true;
                break;
            case 40://↓
                keystate[9] = true;
                break;
            case 13://Enter
                init_view();
                break;
            default:
                break;
        }
    }, false);
    window.addEventListener("keyup", function (e) {
        switch (e.keyCode) {
            case 65://A
                keystate[0] = false;
                break;
            case 87://W
                keystate[1] = false;
                break;
            case 68://D
                keystate[2] = false;
                break;
            case 83://S
                keystate[3] = false;
                break;
            case 81://Q
                keystate[4] = false;
                break;
            case 90://Z
                keystate[5] = false;
                break;
            case 37://←
                keystate[6] = false;
                break;
            case 38://↑
                keystate[7] = false;
                break;
            case 39://→
                keystate[8] = false;
                break;
            case 40://↓
                keystate[9] = false;
                break;
        }
    }, false);
    c.addEventListener("mousewheel", function (e) {
        if (e.wheelDelta < 0 && vstate.scroll < 179) {
            vstate.scroll++;
        }
        else if (e.wheelDelta > 0 && vstate.scroll > 1) {
            vstate.scroll--;
        }
    }, false);
    c.addEventListener("mousedown", function (e) {
        //ホイールボタン
        if (e.button == 1) {
            vstate.scroll = 45;
        }
    }, false);
    c.addEventListener("mousemove", function (e) {
        if (!e) e = window.event; // レガシー

        // マウスの移動量を取得する
        vstate.moment_x = Math.abs(e.movementX) > 40 ? 40 * (e.movementX > 0 ? 1 : -1) : e.movementX || 0;
        vstate.moment_y = 0;//Math.abs(e.movementY) > 40 ? 40 * (e.movementY > 0 ? 1 : -1) : e.movementY || 0;
        vstate.f = true;
    }, false);

    //マウスのポインタをロックするための関数
    function ElementRequestPointerLock(element) {
        var list = [
            "requestPointerLock",
            "webkitRequestPointerLock",
            "mozRequestPointerLock"
        ];
        var i;
        var num = list.length;
        for (i = 0; i < num; i++) {
            if (element[list[i]]) {
                element[list[i]]();
                return true;
            }
        }
        return false;
    }
    //マウスのポインタをロックする
    this.lock_pointer = function () {
        ElementRequestPointerLock(c);
    }

    var vstate = {
        //経度
        latitude: 0,
        //緯度
        longitude: 0,
        x: 50,
        y: 1,
        z: 50,
        scroll: 45,
        moment_x: 0,
        moment_y: 0,
        f: false
    };

    var div;
    this.display_status = function () {
        div = document.createElement("div");
        div.style.position = "absolute";
        div.style.top = "0px";
        div.style.left = "0px";
        div.style.overflow = "hidden";
        div.style.width = "100%";
        div.style.backgroundColor = "rgba(0,255,255,0.5)";
        document.getElementsByTagName("body").item(0).appendChild(div);
    };

    //三角関数の配列
    var s = new (function () {
        this.sin = [];
        this.cos = [];
        this.tan = [];
        for (var i = -360; i <= 360; i++) {
            this.sin[i] = Math.round(Math.sin(i * Math.PI / 180) * 1024) / 1024;
            this.cos[i] = Math.round(Math.cos(i * Math.PI / 180) * 1024) / 1024;
            this.tan[i] = Math.round(Math.tan(i * Math.PI / 180) * 1024) / 1024;
        }

    })();
    //キーボード＆マウスでの視点変更(カメラの頭上がｙ軸固定)
    this.chmview = function () {
        if (keystate[6])
            vstate.latitude = (vstate.latitude + 2) % 360;
        else if (keystate[8])
            vstate.latitude = (vstate.latitude - 2) % 360;
        if (keystate[9] && vstate.longitude > -86)
            vstate.longitude -= 2;
        else if (keystate[7] && vstate.longitude < 86)
            vstate.longitude += 2;
        if (keystate[0]) {//←
            vstate.x -= s.cos[vstate.latitude];
            vstate.z += s.sin[vstate.latitude];
        } else if (keystate[2]) {//→
            vstate.x += s.cos[vstate.latitude];
            vstate.z -= s.sin[vstate.latitude];
        }
        if (keystate[5]) {
            vstate.y--;
        } else if (keystate[4]) {
            vstate.y++;
        }
        if (keystate[3]) {
            vstate.x += s.sin[vstate.latitude];
            vstate.z += s.cos[vstate.latitude];
        } else if (keystate[1]) {
            vstate.x -= s.sin[vstate.latitude];
            vstate.z -= s.cos[vstate.latitude];
        }
        var center = [
            vstate.x - s.cos[vstate.longitude] * s.sin[vstate.latitude],
            vstate.y + s.sin[vstate.longitude],
            vstate.z - s.cos[vstate.longitude] * s.cos[vstate.latitude]
        ];
        m.lookAt([vstate.x, vstate.y, vstate.z], center, [0, 1, 0], vMatrix);
        m.perspective(vstate.scroll, c.width / c.height, 0.1, 200, pMatrix);
        m.multiply(pMatrix, vMatrix, tmpMatrix);

    }

    //マウスによる視点変更
    //返り値にカメラの座標を返す
    this.chmviewbymouse = function () {
        if (vstate.f) {
            vstate.f = false;
            vstate.latitude = (vstate.latitude - Math.floor(vstate.moment_x / 4)) % 360;
            if (vstate.moment_y < 0 && vstate.longitude > 60) vstate.longitude = 60;
            else if (vstate.moment_y > 0 && vstate.longitude < -60) vstate.longitude = -60;
            else vstate.longitude -= Math.floor(vstate.moment_y / 4);
        }
        if (keystate[0]) {//←
            vstate.x -= s.cos[vstate.latitude] / 2;
            vstate.z += s.sin[vstate.latitude] / 2;
        } else if (keystate[2]) {//→
            vstate.x += s.cos[vstate.latitude] / 2;
            vstate.z -= s.sin[vstate.latitude] / 2;
        }
        if (keystate[5]) {
            vstate.y--;
        } else if (keystate[4]) {
            vstate.y++;
        }
        if (keystate[3]) {
            vstate.x += s.sin[vstate.latitude] / 2;
            vstate.z += s.cos[vstate.latitude] / 2;
        } else if (keystate[1]) {
            vstate.x -= s.sin[vstate.latitude] / 2;
            vstate.z -= s.cos[vstate.latitude] / 2;
        }
        var center = [
            vstate.x - s.cos[vstate.longitude] * s.sin[vstate.latitude],
            vstate.y + s.sin[vstate.longitude],
            vstate.z - s.cos[vstate.longitude] * s.cos[vstate.latitude]
        ];
        m.lookAt([vstate.x, vstate.y, vstate.z], center, [0, 1, 0], vMatrix);
        m.perspective(vstate.scroll, c.width / c.height, 0.1, 200, pMatrix);
        m.multiply(pMatrix, vMatrix, tmpMatrix);

        return [vstate.x, vstate.y, vstate.z];
    }

    var init_view = function (view) {
        vstate = {
            //経度
            latitude: 0,
            //緯度
            longitude: 0,
            x: 0,
            y: 0,
            z: 20,
            scroll: 45
        };
        m.lookAt([vstate.x, vstate.y, vstate.z], [0, 0, 19], [0, 1, 0], vMatrix);
        m.perspective(vstate.scroll, c.width / c.height, 0.1, 200, pMatrix);
        m.multiply(pMatrix, vMatrix, tmpMatrix);

        div.innerHTML = JSON.stringify(vstate);
    };

    this.enable_culling = function () {
        gl.enable(gl.CULL_FACE);
    };
    this.disable_culling = function () {
        gl.disable(gl.CULL_FACE);
    };
    this.enable_depth_test = function () {
        gl.enable(gl.DEPTH_TEST);
    };
    this.disable_depth_test = function () {
        gl.disable(gl.DEPTH_TEST);
    };
    //カリング
    gl.frontFace(gl.CCW);
    // 深度テストの比較方法を指定
    gl.depthFunc(gl.LEQUAL);
    //ブレンディング
    //gl.enable(gl.BLEND);
    blend_type(1)

    //四角形の生成
    this.create_quadrangle = function (dx, dz, r, g, b) {
        return new (function () {
            // 頂点属性を格納する配列
            var position = [
                 0.0, 0.0, 0.0,
                 dx, 0.0, 0.0,
                 dx, 0.0, dz,
                 0.0, 0.0, dz
            ];
            var normal = [
                 0, 1, 0,
                 0, 1, 0,
                 0, 1, 0,
                 0, 1, 0
            ];
            var color = [
                r, g, b, 1.0,
                r, g, b, 1.0,
                r, g, b, 1.0,
                r, g, b, 1.0
            ];

            // 頂点のインデックスを格納する配列
            var index = [
                0, 2, 1,
                0, 3, 2
            ];

            // VBOの生成
            var Position = create_vbo(position);
            var Normal = create_vbo(normal);
            var Color = create_vbo(color);

            var VBOList = [Position, Normal, Color];

            // IBOの生成
            var ibo = create_ibo(index);

            var mMatrix = m.identity(m.create());
            var invMatrix = m.identity(m.create());

            this.translate = function (x, y, z) {
                m.translate(mMatrix, [x, y, z], mMatrix);
            };
            this.rotate = function (deg, axis) {
                var rad = (deg % 360) * Math.PI / 180;
                m.rotate(mMatrix, rad, axis, mMatrix);
            }
            this.draw = function () {
                // VBOとIBOをセット
                set_attribute(VBOList, attLocation, attStride);
                gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, ibo);

                gl.uniform1i(uniLocation[8], false);

                m.multiply(tmpMatrix, mMatrix, mvpMatrix);
                // モデル座標変換行列から逆行列を生成
                m.inverse(mMatrix, invMatrix);

                // uniform変数の登録と描画
                gl.uniformMatrix4fv(uniLocation[0], false, mvpMatrix);
                gl.uniformMatrix4fv(uniLocation[1], false, mMatrix);
                gl.uniformMatrix4fv(uniLocation[2], false, invMatrix);
                gl.drawElements(gl.TRIANGLES, index.length, gl.UNSIGNED_SHORT, 0);
            }
            this.init = function () {
                //モデル行列の初期化
                m.identity(mMatrix);
            }

        })();
    }

    //直方体の生成
    this.create_rectangular = function (dx, dy, dz, r, g, b) {
        return new (function () {
            // 頂点属性を格納する配列
            var position = [
                 0.0, 0.0, 0.0,
                 dx, 0.0, 0.0,
                 dx, dy, 0.0,
                 0.0, dy, 0.0,
                 0.0, 0.0, dz,
                 dx, 0.0, dz,
                 dx, dy, dz,
                 0.0, dy, dz
            ];
            var normal = [
                 -1, -1, -1,
                 1, -1, -1,
                 1, 1, -1,
                 -1, 1, -1,
                 -1, -1, 1,
                 1, -1, 1,
                 1, 1, 1,
                 -1, 1, 1
            ];
            var color = [
                r, g, b, 1.0,
                r, g, b, 1.0,
                r, g, b, 1.0,
                r, g, b, 1.0,
                r, g, b, 1.0,
                r, g, b, 1.0,
                r, g, b, 1.0,
                r, g, b, 1.0
            ];

            // 頂点のインデックスを格納する配列
            var index = [
                0, 2, 1,
                0, 3, 2,
                0, 1, 5,
                0, 5, 4,
                1, 6, 5,
                1, 2, 6,
                3, 6, 2,
                3, 7, 6,
                0, 7, 3,
                0, 4, 7,
                4, 5, 6,
                4, 6, 7
            ];

            // VBOの生成
            var Position = create_vbo(position);
            var Normal = create_vbo(normal);
            var Color = create_vbo(color);

            var VBOList = [Position, Normal, Color];

            // IBOの生成
            var ibo = create_ibo(index);

            var mMatrix = m.identity(m.create());
            var invMatrix = m.identity(m.create());

            this.translate = function (x, y, z) {
                m.translate(mMatrix, [x, y, z], mMatrix);
            };
            this.rotate = function (deg, xyz) {
                var rad = (deg % 360) * Math.PI / 180;
                m.rotate(mMatrix, rad, [0, 1, 0], mMatrix);
            }
            this.draw = function () {
                // VBOとIBOをセット
                set_attribute(VBOList, attLocation, attStride);
                gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, ibo);

                gl.uniform1i(uniLocation[8], false);

                m.multiply(tmpMatrix, mMatrix, mvpMatrix);
                // モデル座標変換行列から逆行列を生成
                m.inverse(mMatrix, invMatrix);

                // uniform変数の登録と描画
                gl.uniformMatrix4fv(uniLocation[0], false, mvpMatrix);
                gl.uniformMatrix4fv(uniLocation[1], false, mMatrix);
                gl.uniformMatrix4fv(uniLocation[2], false, invMatrix);
                gl.drawElements(gl.TRIANGLES, index.length, gl.UNSIGNED_SHORT, 0);
            }
            this.init = function () {
                //モデル行列の初期化
                m.identity(mMatrix);
            }

        })();
    }

    //テクスチャ付き四角形の生成
    this.create_texture = function (source) {
        return new (function () {
            // 頂点の位置
            var position = [
                -10.0, 10.0, 0.0,
                 10.0, 10.0, 0.0,
                -10.0, -10.0, 0.0,
                 10.0, -10.0, 0.0
            ];
            var normal = [
                 0, 0, 1,
                 0, 0, 1,
                 0, 0, 1,
                 0, 0, 1
            ];

            // 頂点色
            var color = [
                1.0, 1.0, 1.0, 1.0,
                1.0, 1.0, 1.0, 1.0,
                1.0, 1.0, 1.0, 1.0,
                1.0, 1.0, 1.0, 1.0
            ];

            // テクスチャ座標
            var textureCoord = [
                0.0, 0.0,
                1.0, 0.0,
                0.0, 1.0,
                1.0, 1.0
            ];

            // 頂点インデックス
            var index = [
                0, 2, 1,
                2, 3, 1
            ];

            // VBOの生成
            var Position = create_vbo(position);
            var Normal = create_vbo(normal);
            var Color = create_vbo(color);
            var vTextureCoord = create_vbo(textureCoord);
            var VBOList = [Position, Normal, Color, vTextureCoord];
            var ibo = create_ibo(index);

            var mMatrix = m.identity(m.create());
            var invMatrix = m.identity(m.create());

            // テクスチャ用変数の宣言と生成
            var texture = null;
            create_texture(source, 0);

            this.translate = function (x, y, z) {
                m.translate(mMatrix, [x, y, z], mMatrix);
            };
            this.rotate = function (deg, xyz) {
                var rad = (deg % 360) * Math.PI / 180;
                m.rotate(mMatrix, rad, [0, 1, 0], mMatrix);
            }
            this.draw = function () {
                // VBOとIBOをセット
                set_attribute(VBOList, attLocation, attStride);
                gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, ibo);

                gl.uniform1f(uniLocation[9], 1);
                gl.uniform1i(uniLocation[8], true);

                // テクスチャユニットを指定してバインドし登録する
                gl.activeTexture(gl.TEXTURE0);
                gl.bindTexture(gl.TEXTURE_2D, texture);
                gl.uniform1i(uniLocation[6], 0);

                m.multiply(tmpMatrix, mMatrix, mvpMatrix);
                // モデル座標変換行列から逆行列を生成
                m.inverse(mMatrix, invMatrix);

                // uniform変数の登録と描画
                gl.uniformMatrix4fv(uniLocation[0], false, mvpMatrix);
                gl.uniformMatrix4fv(uniLocation[1], false, mMatrix);
                gl.uniformMatrix4fv(uniLocation[2], false, invMatrix);
                gl.drawElements(gl.TRIANGLES, index.length, gl.UNSIGNED_SHORT, 0);

                //モデル行列の初期化
                m.identity(mMatrix);
            }

            function create_texture(source) {
                // イメージオブジェクトの生成
                var img = new Image();

                // データのオンロードをトリガーにする
                img.onload = function () {
                    // テクスチャオブジェクトの生成
                    var tex = gl.createTexture();

                    // テクスチャをバインドする
                    gl.bindTexture(gl.TEXTURE_2D, tex);

                    // テクスチャへイメージを適用
                    gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, img);

                    // ミップマップを生成
                    gl.generateMipmap(gl.TEXTURE_2D);

                    // テクスチャのバインドを無効化
                    gl.bindTexture(gl.TEXTURE_2D, null);

                    texture = tex;

                };

                // イメージオブジェクトのソースを指定
                img.src = source;
            }
        })();
    }

    //その他の図形の生成
    this.create_sonota = function (position, color, normal, index) {
        return new (function () {
            // VBOの生成
            var Position = create_vbo(position);
            var Normal = create_vbo(normal);
            var Color = create_vbo(color);

            var VBOList = [Position, Normal, Color];

            // IBOの生成
            var ibo = create_ibo(index);

            var mMatrix = m.identity(m.create());
            var invMatrix = m.identity(m.create());

            this.translate = function (x, y, z) {
                m.translate(mMatrix, [x, y, z], mMatrix);
            };
            this.rotate = function (deg, xyz) {
                var rad = (deg % 360) * Math.PI / 180;
                m.rotate(mMatrix, rad, [0, 1, 0], mMatrix);
            }
            this.draw = function () {
                // VBOとIBOをセット
                set_attribute(VBOList, attLocation, attStride);
                gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, ibo);

                gl.uniform1i(uniLocation[8], false);

                m.multiply(tmpMatrix, mMatrix, mvpMatrix);
                // モデル座標変換行列から逆行列を生成
                m.inverse(mMatrix, invMatrix);

                // uniform変数の登録と描画
                gl.uniformMatrix4fv(uniLocation[0], false, mvpMatrix);
                gl.uniformMatrix4fv(uniLocation[1], false, mMatrix);
                gl.uniformMatrix4fv(uniLocation[2], false, invMatrix);
                gl.drawElements(gl.TRIANGLES, index.length, gl.UNSIGNED_SHORT, 0);
            }
            this.init = function () {
                //モデル行列の初期化
                m.identity(mMatrix);
            }
        })();
    }

    //線の生成
    this.create_line = function (x1, y1, x2, y2) {
        return new (function () {
            // 線の頂点位置
            var position = [
                -1.0, -1.0, 0.0,
                 1.0, -1.0, 0.0,
                -1.0, 1.0, 0.0,
                 1.0, 1.0, 0.0
            ];
            var normal = [
                 0, 0, 0,
                 0, 0, 0,
                 0, 0, 0,
                 0, 0, 0,
            ];
            // 線の頂点色
            var color = [
                 1.0, 1.0, 1.0, 1.0,
                 1.0, 0.0, 0.0, 1.0,
                 0.0, 1.0, 0.0, 1.0,
                 0.0, 0.0, 1.0, 1.0
            ];

            // VBOの生成
            var Position = create_vbo(position);
            var Normal = create_vbo(normal);
            var Color = create_vbo(color);

            var VBOList = [Position, Normal, Color];

            var mMatrix = m.identity(m.create());
            var invMatrix = m.identity(m.create());

            this.translate = function (x, y, z) {
                m.translate(mMatrix, [x, y, z], mMatrix);
            };
            this.rotate = function (deg, xyz) {
                var rad = (deg % 360) * Math.PI / 180;
                m.rotate(mMatrix, rad, [0, 1, 0], mMatrix);
            }
            this.draw = function () {
                // 線のプリミティブタイプを判別
                var lineOption = gl.LINE_LOOP;

                // VBOとIBOをセット
                set_attribute(VBOList, attLocation, attStride);

                gl.uniform1i(uniLocation[8], false);

                m.multiply(tmpMatrix, mMatrix, mvpMatrix);
                // モデル座標変換行列から逆行列を生成
                m.inverse(mMatrix, invMatrix);

                // uniform変数の登録と描画
                gl.uniformMatrix4fv(uniLocation[0], false, mvpMatrix);
                gl.uniformMatrix4fv(uniLocation[1], false, mMatrix);
                gl.uniformMatrix4fv(uniLocation[2], false, invMatrix);
                gl.drawArrays(lineOption, 0, position.length / 3);
            }
            this.init = function () {
                //モデル行列の初期化
                m.identity(mMatrix);
            }

        })();
    }

    //トーラスの生成
    this.create_torus = function () {
        return new (function () {

            // トーラスの頂点データからVBOを生成し配列に格納
            var torusData = torus(64, 64, 0.5, 1.5, [0.75, 0.25, 0.25, 1.0]);
            var Position = create_vbo(torusData.p);
            var Normal = create_vbo(torusData.n);
            var Color = create_vbo(torusData.c);
            var VBOList = [Position, Normal, Color];

            // トーラス用IBOの生成
            var ibo = create_ibo(torusData.i);

            var mMatrix = m.identity(m.create());
            var invMatrix = m.identity(m.create());

            this.translate = function (x, y, z) {
                m.translate(mMatrix, [x, y, z], mMatrix);
            };
            this.rotate = function (deg) {
                var rad = (deg % 360) * Math.PI / 180;
                m.rotate(mMatrix, rad, [1, 0, 0], mMatrix);
            }
            this.draw = function () {
                // VBOとIBOをセット
                set_attribute(VBOList, attLocation, attStride);
                gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, ibo);

                m.multiply(tmpMatrix, mMatrix, mvpMatrix);
                // モデル座標変換行列から逆行列を生成
                m.inverse(mMatrix, invMatrix);

                // uniform変数の登録と描画
                gl.uniformMatrix4fv(uniLocation[0], false, mvpMatrix);
                gl.uniformMatrix4fv(uniLocation[1], false, mMatrix);
                gl.uniformMatrix4fv(uniLocation[2], false, invMatrix);
                gl.drawElements(gl.TRIANGLES, torusData.i.length, gl.UNSIGNED_SHORT, 0);

                //モデル行列の初期化
                m.identity(mMatrix);
            }
            // トーラスを生成する関数
            function torus(row, column, irad, orad, color) {
                var pos = new Array(), nor = new Array(),
                    col = new Array(), idx = new Array();
                for (var i = 0; i <= row; i++) {
                    var r = Math.PI * 2 / row * i;
                    var rr = Math.cos(r);
                    var ry = Math.sin(r);
                    for (var ii = 0; ii <= column; ii++) {
                        var tr = Math.PI * 2 / column * ii;
                        var tx = (rr * irad + orad) * Math.cos(tr);
                        var ty = ry * irad;
                        var tz = (rr * irad + orad) * Math.sin(tr);
                        var rx = rr * Math.cos(tr);
                        var rz = rr * Math.sin(tr);
                        if (color) {
                            var tc = color;
                        } else {
                            tc = hsva(360 / column * ii, 1, 1, 1);
                        }
                        pos.push(tx, ty, tz);
                        nor.push(rx, ry, rz);
                        col.push(tc[0], tc[1], tc[2], tc[3]);
                    }
                }
                for (i = 0; i < row; i++) {
                    for (ii = 0; ii < column; ii++) {
                        r = (column + 1) * i + ii;
                        idx.push(r, r + column + 1, r + 1);
                        idx.push(r + column + 1, r + column + 2, r + 1);
                    }
                }
                return { p: pos, n: nor, c: col, i: idx };
            }
        })();
    }

    //球体の生成
    this.create_sphere = function () {
        return new (function () {
            // 球体の頂点データからVBOを生成し配列に格納
            var sphereData = sphere(64, 64, 2.0, [0.25, 0.75, 0.25, 1.0]);
            var Position = create_vbo(sphereData.p);
            var Normal = create_vbo(sphereData.n);
            var Color = create_vbo(sphereData.c);
            var VBOList = [Position, Normal, Color];

            // 球体用IBOの生成
            var ibo = create_ibo(sphereData.i);

            var mMatrix = m.identity(m.create());
            var invMatrix = m.identity(m.create());

            this.translate = function (x, y, z) {
                m.translate(mMatrix, [x, y, z], mMatrix);
            };
            this.rotate = function (deg) {
                var rad = (deg % 360) * Math.PI / 180;
                m.rotate(mMatrix, rad, [0, 1, 0], mMatrix);
            }
            this.draw = function () {
                // VBOとIBOをセット
                set_attribute(VBOList, attLocation, attStride);
                gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, ibo);
                gl.uniform1i(uniLocation[8], false);
                m.multiply(tmpMatrix, mMatrix, mvpMatrix);
                // モデル座標変換行列から逆行列を生成
                m.inverse(mMatrix, invMatrix);

                // uniform変数の登録と描画
                gl.uniformMatrix4fv(uniLocation[0], false, mvpMatrix);
                gl.uniformMatrix4fv(uniLocation[1], false, mMatrix);
                gl.uniformMatrix4fv(uniLocation[2], false, invMatrix);
                gl.drawElements(gl.TRIANGLES, sphereData.i.length, gl.UNSIGNED_SHORT, 0);

                //モデル行列の初期化
                m.identity(mMatrix);
            }

            // 球体を生成する関数
            function sphere(row, column, rad, color) {
                var pos = new Array(), nor = new Array(),
                    col = new Array(), idx = new Array();
                for (var i = 0; i <= row; i++) {
                    var r = Math.PI / row * i;
                    var ry = Math.cos(r);
                    var rr = Math.sin(r);
                    for (var ii = 0; ii <= column; ii++) {
                        var tr = Math.PI * 2 / column * ii;
                        var tx = rr * rad * Math.cos(tr);
                        var ty = ry * rad;
                        var tz = rr * rad * Math.sin(tr);
                        var rx = rr * Math.cos(tr);
                        var rz = rr * Math.sin(tr);
                        if (color) {
                            var tc = color;
                        } else {
                            tc = hsva(360 / row * i, 1, 1, 1);
                        }
                        pos.push(tx, ty, tz);
                        nor.push(rx, ry, rz);
                        col.push(tc[0], tc[1], tc[2], tc[3]);
                    }
                }
                r = 0;
                for (i = 0; i < row; i++) {
                    for (ii = 0; ii < column; ii++) {
                        r = (column + 1) * i + ii;
                        idx.push(r, r + 1, r + column + 2);
                        idx.push(r, r + column + 2, r + column + 1);
                    }
                }
                return { p: pos, n: nor, c: col, i: idx };
            }
        })();
    }

    var vertex = function () {
        var f = function () {
            var VBOList = [], ftexture = false;
            //VBO,IBOを生成する
            this.resist_vertex = function (Position, Normal, Color, IBO) {

                // VBOの生成
                var Position = create_vbo(position);
                var Normal = create_vbo(normal);
                var Color = create_vbo(color);
                VBOList = [Position, Normal, Color];

                ibo = create_ibo(IBO)
            }
            //テクスチャをVBOに登録
            this.resist_texture = function (textureCoord) {
                VBOList.push(create_vbo(textureCoord));
                ftexture = true;
            }
            // VBOとIBOをセット
            this.set_vi = function () {
                set_attribute(VBOList, attLocation, attStride);
                gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, ibo);
            }
            // テクスチャユニットを指定してバインドし登録する
            this.set_texture = function (texture) {
                gl.activeTexture(gl.TEXTURE0);
                gl.bindTexture(gl.TEXTURE_2D, texture);
                gl.uniform1i(uniLocation[6], 0);
            }

            //描画
            this.draw = function () {
                gl.uniform1i(uniLocation[8], false);
                m.multiply(tmpMatrix, mMatrix, mvpMatrix);
                // モデル座標変換行列から逆行列を生成
                m.inverse(mMatrix, invMatrix);
                // uniform変数の登録と描画
                gl.uniformMatrix4fv(uniLocation[0], false, mvpMatrix);
                gl.uniformMatrix4fv(uniLocation[1], false, mMatrix);
                gl.uniformMatrix4fv(uniLocation[2], false, invMatrix);
                gl.drawElements(gl.TRIANGLES, index.length, gl.UNSIGNED_SHORT, 0);
            }

            //モデル行列の初期化
            this.init = function () {
                m.identity(mMatrix);
            }


            this.mMatrix = m.identity(m.create());
            this.invMatrix = m.identity(m.create());

            //平行移動
            this.translate = function (x, y, z) {
                m.translate(mMatrix, [x, y, z], mMatrix);
            };
            //回転　xyz:どの軸を中心に回転するか
            this.rotate = function (deg, xyz) {
                var rad = (deg % 360) * Math.PI / 180;
                m.rotate(mMatrix, rad, [0, 1, 0], mMatrix);
            }


            function create_texture(source, f) {
                // イメージオブジェクトの生成
                var img = new Image();

                // データのオンロードをトリガーにする
                img.onload = function () {
                    // テクスチャオブジェクトの生成
                    var tex = gl.createTexture();

                    // テクスチャをバインドする
                    gl.bindTexture(gl.TEXTURE_2D, tex);

                    // テクスチャへイメージを適用
                    gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, img);

                    // ミップマップを生成
                    gl.generateMipmap(gl.TEXTURE_2D);

                    // テクスチャのバインドを無効化
                    gl.bindTexture(gl.TEXTURE_2D, null);

                    texture = tex;
                    (f || function (a) { })(tex);
                };

                // イメージオブジェクトのソースを指定
                img.src = source;
            }

        }
        return new f()
    }

    //HSV から RGB への変換を行なう関数
    function hsva(h, s, v, a) {
        if (s > 1 || v > 1 || a > 1) { return; }
        var th = h % 360;
        var i = Math.floor(th / 60);
        var f = th / 60 - i;
        var m = v * (1 - s);
        var n = v * (1 - s * f);
        var k = v * (1 - s * (1 - f));
        var color = new Array();
        if (!s > 0 && !s < 0) {
            color.push(v, v, v, a);
        } else {
            var r = new Array(v, n, m, m, k, v);
            var g = new Array(k, v, v, n, m, m);
            var b = new Array(m, m, k, v, v, n);
            color.push(r[i], g[i], b[i], a);
        }
        return color;
    }

    // コンテキストの再描画
    this.flush = function () {
        gl.flush();
    }

    // canvasを初期化
    this.clear_canvas = function (r, g, b) {
        gl.clearColor(r, g, b, 1.0);
        gl.clearDepth(1.0);
        gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
    }

    // ブレンドタイプを設定する関数
    function blend_type(prm) {
        switch (prm) {
            // 透過処理
            case 0:
                gl.blendFunc(gl.SRC_ALPHA, gl.ONE_MINUS_SRC_ALPHA);
                break;
                // 加算合成
            case 1:
                gl.blendFunc(gl.SRC_ALPHA, gl.ONE);
                break;
            default:
                break;
        }
    }
    /********************************************************/

    // シェーダを生成する関数
    function create_shader(id) {
        // シェーダを格納する変数
        var shader;

        // HTMLからscriptタグへの参照を取得
        var scriptElement = document.getElementById(id);

        // scriptタグが存在しない場合は抜ける
        if (!scriptElement) { return; }

        // scriptタグのtype属性をチェック
        switch (scriptElement.type) {

            // 頂点シェーダの場合
            case 'x-shader/x-vertex':
                shader = gl.createShader(gl.VERTEX_SHADER);
                break;

                // フラグメントシェーダの場合
            case 'x-shader/x-fragment':
                shader = gl.createShader(gl.FRAGMENT_SHADER);
                break;
            default:
                return;
        }

        // 生成されたシェーダにソースを割り当てる
        gl.shaderSource(shader, scriptElement.text);

        // シェーダをコンパイルする
        gl.compileShader(shader);

        // シェーダが正しくコンパイルされたかチェック
        if (gl.getShaderParameter(shader, gl.COMPILE_STATUS)) {

            // 成功していたらシェーダを返して終了
            return shader;
        } else {

            // 失敗していたらエラーログをアラートする
            alert(gl.getShaderInfoLog(shader));
        }
    }

    // プログラムオブジェクトを生成しシェーダをリンクする関数
    function create_program(vs, fs) {
        // プログラムオブジェクトの生成
        var program = gl.createProgram();

        // プログラムオブジェクトにシェーダを割り当てる
        gl.attachShader(program, vs);
        gl.attachShader(program, fs);

        // シェーダをリンク
        gl.linkProgram(program);

        // シェーダのリンクが正しく行なわれたかチェック
        if (gl.getProgramParameter(program, gl.LINK_STATUS)) {

            // 成功していたらプログラムオブジェクトを有効にする
            gl.useProgram(program);

            // プログラムオブジェクトを返して終了
            return program;
        } else {

            // 失敗していたらエラーログをアラートする
            alert(gl.getProgramInfoLog(program));
        }
    }

    // VBOを生成する関数
    function create_vbo(data) {
        // バッファオブジェクトの生成
        var vbo = gl.createBuffer();

        // バッファをバインドする
        gl.bindBuffer(gl.ARRAY_BUFFER, vbo);

        // バッファにデータをセット
        gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(data), gl.STATIC_DRAW);

        // バッファのバインドを無効化
        gl.bindBuffer(gl.ARRAY_BUFFER, null);

        // 生成した VBO を返して終了
        return vbo;
    }

    // VBOをバインドし登録する関数
    function set_attribute(vbo, attL, attS) {
        // 引数として受け取った配列を処理する
        for (var i in vbo) {
            // バッファをバインドする
            gl.bindBuffer(gl.ARRAY_BUFFER, vbo[i]);

            // attributeLocationを有効にする
            gl.enableVertexAttribArray(attL[i]);

            // attributeLocationを通知し登録する
            gl.vertexAttribPointer(attL[i], attS[i], gl.FLOAT, false, 0, 0);
        }
    }

    // IBOを生成する関数
    function create_ibo(data) {
        // バッファオブジェクトの生成
        var ibo = gl.createBuffer();

        // バッファをバインドする
        gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, ibo);

        // バッファにデータをセット
        gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Int16Array(data), gl.STATIC_DRAW);

        // バッファのバインドを無効化
        gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, null);

        // 生成したIBOを返して終了
        return ibo;
    }

};
